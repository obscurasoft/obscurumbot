package org.obscurasoft.obscurumbot.command;

import java.lang.management.ManagementFactory;
import java.text.NumberFormat;
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.obscurasoft.obscurumbot.Main;
import org.obscurasoft.obscurumbot.bot.OBotHelper;

public class CommandAnswer extends AbstractCommand {

	@Override
	public String getCommandTrigger() {
		return "бот скажи";
	}

	@Override
	public boolean strictTrigger() {
		return false;
	}

	@Override
	public boolean isCommandNormalTrigger() {
		return true;
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		if (!message.toLowerCase().startsWith("бот скажи")) {
			return;
		}
		if (message.toLowerCase().contains(" размер логов")) {
			if (Main.log != null) {
				try {
					long weight = Main.log.length();
					double mbWeight = (((double) weight / 1024D) / 1024D);
					this.sendMessage(channel, "Размер логов: " + String.format("%.3fMb", mbWeight));
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				this.sendMessage(channel, "Логов нет... ");
			}
			return;
		}

		if (message.toLowerCase().contains(" использование памяти")) {
			boolean mod = false;
			for (String s : OBotHelper.mods)
				if (sender.toLowerCase().equals(s))
					mod = true;

			if (mod) {
				NumberFormat format = NumberFormat.getInstance();

				long maxMemory = Runtime.getRuntime().maxMemory();
				long allocatedMemory = Runtime.getRuntime().totalMemory();
				long freeMemory = Runtime.getRuntime().freeMemory();
				long currentUsed = allocatedMemory - freeMemory;
				this.sendMessage(channel, "Использование памяти: " + format.format(currentUsed / 1024 / 1024) + "Mb of "
						+ format.format(maxMemory / 1024 / 1024) + "Mb (" + (currentUsed * 100L / maxMemory) + "%)");
			} else {
				this.sendMessage(channel, "Запрещено.");
			}
			return;
		}
		if (message.toLowerCase().contains(" использование процессора")) {
			boolean mod = false;
			for (String s : OBotHelper.mods)
				if (sender.toLowerCase().equals(s))
					mod = true;

			if (mod) {
				try {
					MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
					ObjectName name = ObjectName.getInstance("java.lang:type=OperatingSystem");
					AttributeList list = mbs.getAttributes(name, new String[] { "ProcessCpuLoad" });
					if (list.isEmpty()) {
						return;
					}
					Attribute att = (Attribute) list.get(0);
					double value = (double) att.getValue();
					double percentageUsed = ((int) (value * 1000) / 10D);
					this.sendMessage(channel, "Использование процессора: " + percentageUsed + "%");
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				this.sendMessage(channel, "Запрещено.");
			}
			return;
		}
		if (message.toLowerCase().contains(" что")) {
			this.sendMessage(channel, "14");
			return;
		}
		if (message.toLowerCase().contains(" 14")) {
			this.sendMessage(channel, "41");
			return;
		}
		if (message.toLowerCase().contains(" moo")) {
			this.sendMessage(channel, "Cow SuperHero!");
			return;
		}
		if (message.toLowerCase().contains("где")) {
			if (message.toLowerCase().contains(" ты")) {
				this.sendMessage(channel, sender + ", я тут. Привет))");
				return;
			}
			if (message.toLowerCase().contains(" Годус")) {
				this.sendMessage(channel, sender + ", если его тут нет, скорее всего он что-то кодит.");
				return;
			}
			if (message.toLowerCase().contains(" кофе")) {
				this.sendMessage(channel, sender + " У меня в кружке.");
				return;
			}
			if (message.toLowerCase().contains(" Вованоксин")) {
				this.sendMessage(channel, sender + ", а чёрть его знает где его круговерть носит...");
				return;
			}
		}
		if (message.toLowerCase().contains(" Годус лучше")) {
			this.sendMessage(channel, "Я всегда буду говорить что он лучше любого в этом мире, ибо он - мой создатель. Славте его!");
		}
	}
}
