package org.obscurasoft.obscurumbot.command;

public class EventResponse extends AbstractCommand {

	@Override
	public String getCommandTrigger() {
		return null;
	}

	@Override
	public boolean strictTrigger() {
		return false;
	}

	@Override
	public boolean isCommandNormalTrigger() {
		return false;
	}

	public boolean trigger(String command, String sender, String channel) {
		return true;
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		if (message.toLowerCase().contains("101%") && sender.equalsIgnoreCase("botbder")) {
			this.sendMessage(channel, "Сурйозно?..");
		}
		if (message.toLowerCase().contains("какие такие ошибки?") && sender.equalsIgnoreCase("godusx")) {
			this.sendMessage(channel, "В коде, дурак!");
		}
		if (message.toLowerCase().contains("это утверждение ложно")) {
			this.sendMessage(channel, "LogicalException! Перезапуск...");
		}
		if (message.toLowerCase().contains("в недрах тундры выдра в гетрах тырит в ведрах ядра кедра")) {
			this.sendMessage(channel,
					"Карл украл у Клары кларнет, Клара украла у Карла желудочно-кишечный тракт. Kappa");
		}
		if (message.toLowerCase().contains("я вернутый")) {
			this.sendMessage(channel, "С развратом");
		}
		if (message.toLowerCase().contains("зачем") || message.toLowerCase().contains("gjxtve")) {
			this.sendMessage(channel, "Потому что гладиолус.");
		}
		if (message.toLowerCase().contains("Торт - ложь") || message.toLowerCase().contains("the cake is a lie")) {
			this.sendMessage(channel, "deIlluminati");
		}
		if (message.toLowerCase().contains("бекон")) {
			this.sendMessage(channel, "Был час ночи, горела свиноферма...");
		}
		if (message.toLowerCase().contains("разведка")) {
			this.sendMessage(channel, " В руках граната, в попе ветка - это к нам ползёт разведка!");
		}
		if (message.toLowerCase().contains("модбдер") && channel.contains("modbder")) {
			this.sendMessage(channel, "Модбдер в чате!");
		} else if (message.toLowerCase().contains("!модбдер") && (!channel.contains("modbder"))) {
			this.sendMessage(channel, "Модбдера с нами нет :с");
		}
		if (message.toLowerCase().contains("землю видно")) {
			this.sendMessage(channel, "Земля в иллюминаторе! Земля в иллюминаторе!..");
		}
		if (message.toLowerCase().contains("я припёрся")) {
			this.sendMessage(channel, "Глядите, " + sender + " внезапно припёрся - о чудо!");
		}
		if (message.toLowerCase().contains("я пошёл кушать")
				&& (sender.equalsIgnoreCase("godusx") || sender.equalsIgnoreCase("vovanoxin"))) {
			this.sendMessage(channel, "Приятного апетита!");
		}
		if ((message.toLowerCase().contains("я пойду") || message.toLowerCase().contains("я пошел")
				|| message.toLowerCase().contains("пойду я")) && sender.equalsIgnoreCase("godusx")) {
			this.sendMessage(channel, "Пока, создатель :(");
		}
		if ((message.toLowerCase().contains("бот остаётся тут") && sender.equalsIgnoreCase("godusx"))) {
			this.sendMessage(channel, "Эй!");
		}
		if ((message.toLowerCase().contains("пойдём, ботбдер") && sender.equalsIgnoreCase("modbder"))) {
			this.sendMessage(channel, "Пока Модбдер, пока Ботбдер.");
		}
		if (message.toLowerCase().contains("пойдём, владичат") && sender.equalsIgnoreCase("vovanoxin")) {
			this.sendMessage(channel, "Пока Владичат, пока Вованоксин.");
		}
		if (message.toLowerCase().contains("лол")) {
			this.sendMessage(channel, "кек");
		}
	}
}
