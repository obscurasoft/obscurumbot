package org.obscurasoft.obscurumbot.command;

public class CommandCat extends AbstractCommand {

	@Override
	public String getCommandTrigger() {
		return "бот ня";
	}

	@Override
	public boolean strictTrigger() {
		return true;
	}

	@Override
	public boolean isCommandNormalTrigger() {
		return true;
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		this.sendMessage(channel, "Не время для няшностей, надо заниматься наукой!");
	}

}
