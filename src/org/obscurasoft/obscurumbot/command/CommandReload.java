package org.obscurasoft.obscurumbot.command;

import org.obscurasoft.obscurumbot.Main;
import org.obscurasoft.obscurumbot.bot.OBotHelper;

public class CommandReload extends AbstractCommand {

	@Override
	public String[] getCommandUsers() {
		return OBotHelper.mods;
	}

	@Override
	public String getCommandTrigger() {
		return "бот перезагрузка";
	}

	@Override
	public boolean strictTrigger() {
		return true;
	}

	@Override
	public boolean isCommandNormalTrigger() {
		return true;
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		Main.obot.reload();
		OBotHelper.messageBot("Перезагрузка завершена!", true);
	}
}
