package org.obscurasoft.obscurumbot.command;

import org.obscurasoft.obscurumbot.Main;
import org.obscurasoft.obscurumbot.bot.OBotHelper;

public class CommandCooldown extends AbstractCommand {

	@Override
	public String[] getCommandUsers() {
		return OBotHelper.mods;
	}

	@Override
	public String getCommandTrigger() {
		return "бот кулдаун";
	}

	@Override
	public boolean strictTrigger() {
		return false;
	}

	@Override
	public boolean isCommandNormalTrigger() {
		return true;
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		if (message.startsWith("бот кулдаун") && message.length() > 10) {
			try {
				int newCldwn = Integer.parseInt(message.substring(10));
				if (newCldwn >= 0) {
					Main.obot.globalCooldown = newCldwn;
					this.sendMessage(channel, "Подготовка - " + newCldwn + " секунд.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (message.startsWith("!cooldown"))
			this.sendMessage(channel, "Подготовка - " + Main.obot.globalCooldown + " секунд.");
	}

}
