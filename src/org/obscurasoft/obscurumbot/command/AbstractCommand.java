package org.obscurasoft.obscurumbot.command;

import org.obscurasoft.obscurumbot.Main;
import org.obscurasoft.obscurumbot.bot.OBotHelper;

public abstract class AbstractCommand {
	/**
	 * Abstract command provider.
	 * 
	 * @return
	 */
	public abstract String getCommandTrigger();

	public String[] getCommandAlias() {
		return new String[0];
	}

	public String[] getCommandUsers() {
		return new String[0];
	}

	public abstract boolean strictTrigger();

	public abstract boolean isCommandNormalTrigger();

	public abstract void performCommand(String message, String sender, String channel);

	public boolean trigger(String command, String sender, String channel) {
		if (getCommandUsers().length > 0)
			if (!arrayStringEquals(getCommandUsers(), sender)) {
				return false;
			}
		if (strictTrigger()) {
			return command.toLowerCase().equals(getCommandTrigger()) || arrayStringEquals(getCommandAlias(), command);
		} else {
			return command.toLowerCase().contains(getCommandTrigger())
					|| arrayStringContains(getCommandAlias(), command);
		}
	}

	public boolean arrayStringEquals(String[] array, String compared) {
		for (String s : array) {
			if (s.toLowerCase().equals(compared)) {
				return true;
			}
		}
		return false;
	}

	public boolean arrayStringContains(String[] array, String compared) {
		for (String s : array) {
			if (s.toLowerCase().contains(compared)) {
				return true;
			}
		}
		return false;
	}

	public void sendMessage(String channel, String message) {
		if (OBotHelper.getBotChannel().equals(channel)) {
			OBotHelper.messageBot(message, false);
		}
		Main.obot.delay = 2;
	}
}
