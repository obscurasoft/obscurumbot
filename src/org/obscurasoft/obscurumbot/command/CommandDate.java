package org.obscurasoft.obscurumbot.command;

import java.util.Calendar;

public class CommandDate extends AbstractCommand {

	@Override
	public String getCommandTrigger() {
		return "бот дата";
	}

	public String[] getCommandAlias() {
		return new String[] { "дата", "date", "какая сегодня дата" };
	}

	@Override
	public boolean strictTrigger() {
		return true;
	}

	@Override
	public boolean isCommandNormalTrigger() {
		return true;
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		Calendar c = Calendar.getInstance();
		this.sendMessage(channel, "Сейчас " + c.getTime().toString());
	}

}
