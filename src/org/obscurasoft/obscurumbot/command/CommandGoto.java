package org.obscurasoft.obscurumbot.command;

import org.obscurasoft.obscurumbot.Main;
import org.obscurasoft.obscurumbot.bot.OBotHelper;

public class CommandGoto extends AbstractCommand {

	@Override
	public String[] getCommandUsers() {
		return OBotHelper.mods;
	}

	@Override
	public String getCommandTrigger() {
		return "бот пошли к";
	}

	@Override
	public boolean strictTrigger() {
		return false;
	}

	@Override
	public boolean isCommandNormalTrigger() {
		return false;
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		try {
			String channelStr = message.substring(9);
			this.sendMessage(channel, "Хорошо, хозяин...");
			Main.obot.partChannel(channel, "Я ушель к другому...");
			Main.obot.joinChannel("#" + channelStr);
			Main.obot.lastChannel = "#" + channelStr;
		} catch (Exception e) {
		}
	}
}
