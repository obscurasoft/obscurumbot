package org.obscurasoft.obscurumbot.command;

import org.obscurasoft.obscurumbot.bot.OBotHelper;

public class EventGreetAll extends AbstractCommand {
	public static boolean seenBotbder = false;
	public static boolean seenGodusx = false;
	public static boolean seenVladichat = false;
	public static boolean seenModbder = false;

	@Override
	public String getCommandTrigger() {
		return "";
	}

	@Override
	public boolean strictTrigger() {
		return false;
	}

	@Override
	public boolean isCommandNormalTrigger() {
		return false;
	}

	@Override
	public void performCommand(String message, String sender, String channel) {

		if (sender.toLowerCase().contains("modbder")) {
			if (!seenModbder) {
				seenModbder = true;
				OBotHelper.messageBot("Всемогущий Модбдер! Преклоняемся перед тобой! ", false);
			}
		}

		if (sender.toLowerCase().contains("vladichat")) {
			if (!seenVladichat) {
				seenVladichat = true;
				OBotHelper.messageBot("Владичат, братишка, привет!", false);
			}
		}

		if (sender.toLowerCase().contains("godusx")) {
			if (!seenGodusx) {
				seenGodusx = true;
				OBotHelper.messageBot("Создатель! Моё почтение!", false);
			}
		}
		if (sender.toLowerCase().contains("botbder")) {
			if (!seenBotbder) {
				seenBotbder = true;
				OBotHelper.messageBot("Ботбдер! Привет, брат по коду!", false);
			}
		}
	}

	public boolean trigger(String command, String sender, String channel) {
		return true;
	}

}
