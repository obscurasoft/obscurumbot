package org.obscurasoft.obscurumbot.thread;

import org.obscurasoft.obscurumbot.Main;

public class BotTick extends Thread {
	/**
	 * Tick thread. Listening loop.
	 */
	public BotTick() {
		super("[OBotTick]ObscurumBot Ticks");
		this.setDaemon(true);
	}

	public void run() {
		super.run();
		while (true) {
			try {
				sleep(1000L);
				if (Main.obot != null) {
					synchronized (Main.obot) {
						Main.obot.mainLoop();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
	}
}
