package org.obscurasoft.obscurumbot.bot;

import java.util.ArrayList;
import java.util.Stack;
import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;
import org.obscurasoft.obscurumbot.Main;
import org.obscurasoft.obscurumbot.command.AbstractCommand;

public class ObscurumBot extends PircBot {
	/**
	 * ObscurumBot core Cooldown, reloading, help command, listening for
	 * messages.
	 */
	public int delay;
	public int botUptime;
	public int cooldown;
	public int help = 0;
	public int globalCooldown = 10;
	public String lastChannel;
	public String lastSender;
	public static final ArrayList<String> messagesToDeliver = new ArrayList<String>();
	public static final ArrayList<AbstractCommand> command = new ArrayList<AbstractCommand>();

	public ObscurumBot() {
		super();
		this.setName(Main.irc[4]);
		System.out.println("Using name " + Main.irc[4]);
		this.setLogin(Main.irc[4]);
		reload();
	}

	public void reload() {
		command.clear();
		Package p = this.getClass().getPackage();
		try {
			String pa = p.getName().substring(0, p.getName().lastIndexOf('.')).concat(".command");
			Class<?>[] commands = OBotHelper.getClasses(pa);
			for (Class<?> c : commands)
				if (c.getSuperclass().equals(AbstractCommand.class)) {
					AbstractCommand command = AbstractCommand.class.cast(c.newInstance());
					ObscurumBot.command.add(command);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		help = 0;
	}

	@Override
	public void onMessage(String channel, String sender, String login, String hostname, String message) {
		boolean ignoreCooldown = globalCooldown <= 0;
		for (String s : OBotHelper.mods) {
			if (sender.toLowerCase().equals(s)) {
				ignoreCooldown = true;
			}
		}
		if (cooldown > 0 && !ignoreCooldown) {
			return;
		}
		int queueSize = messagesToDeliver.size();
		if (message.equalsIgnoreCase("бот")) {
			// Iterating command array
			ArrayList<String> commandsarray = new ArrayList<String>();
			for (AbstractCommand c : command) {
				if (c.isCommandNormalTrigger() && c.getCommandTrigger() != null && !c.getCommandTrigger().isEmpty()) {
					commandsarray.add(c.getCommandTrigger());
				}
			}
			String sended = "Комманды: ";
			int cnt = 0;
			for (String s : commandsarray) {
				if (cnt == 0) {
					sended += " " + s;
				} else {
					sended += ", " + s;
				}
				++cnt;
			}
			this.sendMessage(channel, sended);
			commandsarray.clear();
			commandsarray.trimToSize();
			commandsarray = null;
		}
		if (messagesToDeliver.size() > queueSize && !ignoreCooldown) {
			cooldown = globalCooldown;
		}
	}

	protected void onUserList(String channel, User[] users) {
		Stack<String> nicknames = new Stack<String>();
		for (User u : users) {
			nicknames.push(u.getNick());
		}
	}

	public void mainLoop() {
		// Bot tick loop
		if (delay > 0) {
			--delay;
		}
		--cooldown;
		if (delay == 0) {
			if (!messagesToDeliver.isEmpty()) {
				this.sendMessage(lastChannel, messagesToDeliver.get(0));
				messagesToDeliver.remove(0);
				messagesToDeliver.trimToSize();
			}
		}
		++botUptime;
	}
}
