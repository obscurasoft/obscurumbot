package org.obscurasoft.obscurumbot;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class LogStream extends PrintStream {
	/**
	 * Logging stream Overrides close(), flush(), write().
	 */
	private final PrintStream secStr;

	public LogStream(OutputStream main, PrintStream sec) {
		super(main);
		this.secStr = sec;
	}

	@Override
	public void close() {
		super.close();
	}

	@Override
	public void flush() {
		super.flush();
		secStr.flush();
	}

	@Override
	public void write(byte[] buf, int off, int len) {
		super.write(buf, off, len);
		secStr.write(buf, off, len);
	}

	@Override
	public void write(int b) {
		super.write(b);
		secStr.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException {
		super.write(b);
		secStr.write(b);
	}
}
