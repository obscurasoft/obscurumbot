package org.obscurasoft.obscurumbot;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Scanner;

import org.obscurasoft.obscurumbot.bot.ObscurumBot;
import org.obscurasoft.obscurumbot.thread.BotTick;

public class Main {
	/**
	 * Main file Starting logging, starting ticking, connecting.
	 */
	public static ObscurumBot obot;
	public static final String[] irc = new String[5];
	public static File log;

	public static void main(String[] args) {
		try {
			while (true) {
				int i = 0;
				File config = new File("ObscurumBot.cfg");
				if (!config.exists()) {
					config.createNewFile();
					FileOutputStream secFile = new FileOutputStream(config);
					LogStream sec = new LogStream(secFile, System.out);
					System.setOut(sec);
					System.out.println("obscurum.ddns.net\n6667\npass\n#test\nname");
					sec = null;
					secFile = null;
				}
				@SuppressWarnings("resource")
				Scanner in = new Scanner(config);
				while (in.hasNextLine()) {
					irc[i] = in.nextLine();
					i++;
				}
				config = null;
				break;
			}
			// Start ticking
			new BotTick().start();
			obot = new ObscurumBot();
			try {
				// Creating file for logging
				File file = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath())
						.getParentFile();
				File logPlace = new File(file, "log");
				if (!logPlace.exists()) {
					logPlace.mkdir();
				}
				File output = new File(logPlace, "ObscurumBot.log");
				if (!output.exists())
					output.createNewFile();
				log = output;

				FileOutputStream anotherFile = new FileOutputStream(output);
				LogStream los = new LogStream(anotherFile, System.out);
				System.setOut(los);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// Connecting to IRC server
			obot.setVerbose(true);
			System.out.println("Connecting to " + irc[0] + ":" + irc[1]);
			obot.connect(irc[0], Integer.parseInt(irc[1]), irc[2]);
			System.out.println("Joining to channel " + irc[3]);
			obot.joinChannel(irc[3]);
			obot.lastChannel = irc[3];
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
